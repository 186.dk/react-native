import React, {Component} from "react";
import {StyleSheet, Text} from "react-native";

export default  class BlinkText extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showText: true
        };
        let taskTodo = () => {
            this.setState(previousState => {
                return {
                    showText: !previousState.showText
                }
            })
        };
        const timeToBlink = 1000;
        setInterval(taskTodo, timeToBlink);
    }
    render() {
        let textToDisplay = this.state.showText ? this.props.inputText: '';
        return (
            <Text style = {styles.blinkText}>{textToDisplay}</Text>
        );
    }
}

const styles = StyleSheet.create({
    blinkText: {
        fontSize: 16,
        marginLeft: 15,
        marginTop: 15,
        marginBottom: 12,
    },
});

/*
Mr Nguyen Duc Hoang
https://www.youtube.com/c/nguyenduchoang
Email: sunlight4d@gmail.com
LifeCycle Component => example of LifeCycle in React Native
*/

import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, Platform} from 'react-native';

class LifeCycle extends Component {
    _isMounted = false;
    _intervalId = 0;
    constructor(props) {
        super(props);
        console.log(`${Date.now()}. This is constructor`);
        this.state = {
            numberOfRefresh: 0
        };
    }

    /**
     * Run once on life cycle
     */
    componentWillMount() {
        console.log(`${Date.now()}. This is componentWillMount`);
    }

    componentDidMount() {
        this._isMounted = true;
        console.log(`${Math.floor(Date.now())}. This is componentDidMount`);

       this._intervalId = setInterval(() => {
            console.log(`${Date.now()}. Change State each 2 seconds`);
            if (this._isMounted){
                this.setState(previousState => {
                    return {numberOfRefresh: previousState.numberOfRefresh + 1};
                });
            }
        }, 2000);
    }

    componentWillUnmount() {
        clearInterval(this._intervalId);
        this._isMounted = false;
    }

    /**
     * Run loops (multi times) on life cycle
     * @returns {boolean}
     */
    shouldComponentUpdate() {
        console.log(`${Date.now()}. This is shouldComponentUpdate`);
        // if false, the component will execute neither willUpdate nor didUpdate functions
        return true;
    }

    componentWillUpdate() {
        console.log(`${Date.now()}. This is componentWillUpdate`);
    }

    componentDidUpdate() {
        console.log(`${Date.now()}. This is componentDidUpdate`);
    }

    render() {
        console.log(`${Math.floor(Date.now())}. This is render function`);
        let textToDisplay = `Numbers of refresh: ${this.state.numberOfRefresh}`;
        return (
            <View>
                <Text>LifeCycle test: check console log for more detail</Text>
                <Text>{textToDisplay}</Text>
            </View>
        );
    }
}

export default class LifeCycleComponent extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        var lifeCycle = <LifeCycle propA="abc"></LifeCycle>;
        return (
            <View>
                {lifeCycle}
            </View>
        );
    }
}
/*
Mr Nguyen Duc Hoang
https://www.youtube.com/c/nguyenduchoang
Email: sunlight4d@gmail.com
This contains functions, that send GET, POST, DELETE, PUT requests to server
*/
import React, { Component } from 'react';
const apiGetAllFoods = 'https://gitlab.com/186.dk/react-native/raw/master/demos/BasicFlatList/list.json';
async function getFoodsFromServer() {
    try {
        let response = await fetch(apiGetAllFoods);
        // git lab row is text
        let responseText = await response.text();
        let responseJson = JSON.parse(responseText);
        console.log(responseJson);
        return responseJson; //list of foods
    } catch (error) {
        console.error(`Error is : ${error}`);
    }
}
export {getFoodsFromServer};

//send post request to insert new data, unimplemented
const apiInsertNewFood = 'http://localhost:3001/insert_new_food';
async function insertNewFoodToServer(params) {
    try {
        let response = await fetch(apiInsertNewFood, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        return responseJson.result;
    } catch (error) {
        console.error(`Error is : ${error}`);
    }
}
export {insertNewFoodToServer};

//send post request to update new data, unimplemented
const apiUpdateAFood = 'http://localhost:3001/update_a_food';
async function updateAFood(params) {
    try {
        let response = await fetch(apiUpdateAFood, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(params)
        });
        let responseJson = await response.json();
        return responseJson.result;
    } catch (error) {
        console.error(`Error is : ${error}`);
    }
}
export {updateAFood};
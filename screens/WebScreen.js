import * as WebBrowser from 'expo-web-browser';
import React, {Component} from 'react';

import {
    StyleSheet,
    Keyboard,
    WebView,
    ActivityIndicator,
    View,
    Text,
    TextInput,
    Button
} from 'react-native';


export default class WebScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingVisible: true,
            myUrl: '',
            reload: 0
        };
    }

    /**
     * update text input url
     */
    updateUrl() {
        let newUrl = this.state.TextInputValue;
        newUrl = (typeof (newUrl) === 'undefined') ? '' : newUrl;
        this.setState({myUrl: newUrl})
    }

    /**
     * show pending spinner
     */
    showSpinner() {
        console.log('Show Spinner');
        this.setState({loadingVisible: true});
    }

    hideSpinner() {
        console.log('Hide Spinner');
        this.setState({loadingVisible: false});
    }

    // this must be function
    reload() {
        this.webView.reload();
    };

    goBack() {
        this.webView.goBack();
    };

    goForward() {
        this.webView.goForward();
    };

    stopLoading = () => {
        if (this.webview) {
            this.webview.stopLoading();
        }
    };

    _onNavigationStateChange = (navState) => {
        console.log(navState);
        this.setState({
            backButtonEnabled: navState.canGoBack,
            forwardButtonEnabled: navState.canGoForward,
            url: navState.url,
            TextInputValue: navState.url,
            status: navState.title,
            loading: navState.loading,
        });
    };

    render() {
        let webview;

        if (this.state.myUrl.trim() === "") {
            // You need to center element in View
            webview = <View
                style={styles.centerContent}>
                <Text>Empty page</Text>
            </View>
        } else {

            // check if url
            const pattern = /^((http|https|ftp):\/\/)/;
            let fullUrl = this.state.myUrl;
            if (!pattern.test(fullUrl)) {
                fullUrl = "http://" + fullUrl;
            }

            webview = <View
                style={this.state.loadingVisible === true ? styles.centerContent : styles.styleNew}>
                {
                    this.state.loadingVisible ? (
                        <ActivityIndicator
                            color="#009688"
                            size="large"
                            style={styles.ActivityIndicatorStyle}
                        />
                    ) : null
                }

                <WebView
                    ref={(ref) => this.webView = ref}

                    style={styles.WebViewStyle}
                    //Loading URL
                    source={{uri: fullUrl}}

                    sharedCookiesEnabled={true}
                    thirdPartyCookiesEnabled={true}

                    //Enable Javascript support
                    javaScriptEnabled={true}
                    //For the Cache
                    domStorageEnabled={true}

                    //View to show while loading the webpage
                    //Want to show the view or not
                    //startInLoadingState={true}

                    //use web kit can play sound
                    useWebKit={true}

                    onLoadStart={() => this.showSpinner()}
                    onLoadEnd={() => this.hideSpinner()}

                    //It is not show loading msg in first time
                    renderLoading={() => {
                        return <View style={styles.centerContent}><Text>Loading...</Text></View>
                    }}

                    // renderError={this.renderError}
                    renderError={() => {
                        return <View style={styles.centerContent}>
                            <Text>No connection!</Text>
                            <Button onPress={() => this.reload()} title="Reload"/>
                        </View>
                    }}

                    onNavigationStateChange={this._onNavigationStateChange}
                />
            </View>;
        }

        return (
            // Whole page layout
            <View
                style={{
                    flex: 1,
                }}
            >
                <View style={
                    {backgroundColor: 'yellow'}
                }>
                    {/*
                * navigator bar layout start
                */}
                    <View style={{
                        flexDirection: 'row',
                        height: 30,
                    }}>
                        <TextInput
                            style={styles.Input}
                            onChangeText={TextInputValue => this.setState({TextInputValue})}
                            value={this.state.TextInputValue}
                            autoFocus={true}
                            autoCapitalize="none"
                            returnKeyType={"go"}
                            onKeyPress={(e) => {
                                console.log(e.nativeEvent);
                            }}

                            // press go button
                            onSubmitEditing={
                                () => {
                                    Keyboard.dismiss();
                                    this.updateUrl();
                                }}

                            placeholder="Enter url"
                        >
                        </TextInput>
                        <Button
                            style={{
                                width: 20
                            }}
                            onPress={() => {
                                Keyboard.dismiss();
                                this.updateUrl();
                            }}
                            title="Go"
                        />
                    </View>
                    {/*
                * navigator layout end
                */}

                    {/*
                * tools layout start
                */}
                    <View style={{
                        flexDirection: 'row',
                        height: 30,
                        // cannot use flex here, it will overwrite height value
                        // flex: 1
                    }}>
                        <Button
                            onPress={() => this.reload()}
                            title="Reload"
                        />
                        <Button
                            onPress={() => this.goBack()}
                            title="Back"
                        />
                        <Button
                            onPress={() => this.goForward()}
                            title="Forward"
                        />
                        <Button
                            onPress={this.stopLoading}
                            title="Stop"
                        />
                    </View>
                    {/*
                * tools layout end
                */}
                </View>

                {/*
                Webview part
                */}
                {webview}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    // container: {
    //     flex: 1,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     backgroundColor: '#F5FCFF',
    // },
    centerText: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    centerContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    styleNew: {
        flex: 1,
    },
    WebViewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    ActivityIndicatorStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
    Input: {
        margin: 10,
        height: 20,
        width: 250,
        borderColor: '#7a42f4',
        borderWidth: 1
    },
});



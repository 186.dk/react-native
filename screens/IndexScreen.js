import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {StyleSheet, Image, Text, View, ScrollView} from 'react-native';
import Touchable from 'react-native-platform-touchable';
import {Ionicons} from '@expo/vector-icons';

class ItemLink extends React.Component {
    render() {
        return (
            <Touchable
                style={styles.option}
                background={Touchable.Ripple('#ccc', false)}
                onPress={() => {
                    let {navigate} = this.props.navigation;
                    navigate(this.props.linkScreen);
                }}
            >
                <View style={{flexDirection: 'row'}}>
                    <View style={styles.optionIconContainer}>
                        {/*more icon in https://expo.github.io/vector-icons/*/}
                        {/*only Ionicons can works*/}
                        <Ionicons name="md-arrow-round-forward" size={23} color="blue"/>
                    </View>
                    <View>
                        <Text style={styles.optionText}>{this.props.text}</Text>
                    </View>
                </View>
            </Touchable>
        );
    }
}

// here show all links
class LinkItems extends React.Component {
    render() {
        return (
            <ScrollView>
                <ItemLink linkScreen='Home' text="Home" navigation={this.props.navigation}/>
                <ItemLink linkScreen='Settings' text="Settings" navigation={this.props.navigation}/>
                <ItemLink linkScreen='Links' text="Links" navigation={this.props.navigation}/>
                <ItemLink linkScreen='Web' text="Web view demo" navigation={this.props.navigation}/>
                <ItemLink linkScreen='FlatList' text="Basic flat list demo" navigation={this.props.navigation}/>
                <ItemLink linkScreen='BasicFlatListRemote' text="Basic flat list remote demo" navigation={this.props.navigation}/>
                <ItemLink linkScreen='LifeCycleComponent' text="Life cycle demo" navigation={this.props.navigation}/>
            </ScrollView>
        )
    }
}

export default class IndexScreen extends React.Component {
    render() {
        // console.log(this);
        return (
            <View  style={styles.container}>
                <Text style={styles.optionsTitleText}>Menu</Text>
                <LinkItems navigation={this.props.navigation}/>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    optionsTitleText: {
        fontSize: 16,
        marginLeft: 15,
        marginTop: 9,
        marginBottom: 12,
    },
    optionIconContainer: {
        marginRight: 9,
    },
    option: {
        backgroundColor: '#fdfdfd',
        paddingHorizontal: 15,
        paddingVertical: 15,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#EDEDED',
    },
    optionText: {
        fontSize: 15,
        marginTop: 1,
    },
});

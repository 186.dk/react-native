import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import IndexScreen from '../screens/IndexScreen';
import WebScreen from '../screens/WebScreen';
import FlatListScreen from "../demos/BasicFlatList/FlatListScreen";

const config = Platform.select({
    web: {headerMode: 'screen'},
    default: {},
});

const HomeStack = createStackNavigator(
    {
        Home: HomeScreen,
    },
    config
);

HomeStack.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === 'ios'
                    ? `ios-information-circle${focused ? '' : '-outline'}`
                    : 'md-information-circle'
            }
        />
    ),
};

HomeStack.path = '';

const LinksStack = createStackNavigator(
    {
        Links: LinksScreen,
    },
    config
);

LinksStack.navigationOptions = {
    tabBarLabel: 'Links',
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}/>
    ),

};

LinksStack.path = '';

const SettingsStack = createStackNavigator(
    {
        Settings: SettingsScreen,
    },
    config
);

SettingsStack.navigationOptions = {
    tabBarLabel: 'Settings',
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}/>
    ),
};

SettingsStack.path = '';

const IndexStack = createStackNavigator(
    {
        Index: IndexScreen,
    },
    config
);

IndexStack.navigationOptions = {
    tabBarLabel: 'Index',
    title: "Index page",
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}/>
    ),
};

const WebStack = createStackNavigator(
    {
        Web: WebScreen,
    },
    config
);

WebStack.navigationOptions = {
    tabBarLabel: 'Web',
    title: "Web page",
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}/>
    ),
};
WebStack.path = '';

const BasicFlatListStack = createStackNavigator(
    {
        FlatList: FlatListScreen,
    },
    config
);

const tabNavigator = createBottomTabNavigator({
    IndexStack,
    HomeStack,
    LinksStack,
    SettingsStack,
    WebStack,
    BasicFlatListStack
});

tabNavigator.path = '';

export default tabNavigator;

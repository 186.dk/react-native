import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import IndexScreen from '../screens/IndexScreen';
import WebScreen from '../screens/WebScreen';
import FlatListScreen from "../demos/BasicFlatList/FlatListScreen";
import BasicFlatListRemote from "../demos/BasicFlatList/BasicFlatListRemote";
import LifeCycleComponent from "../demos/lifeCycle/LifeCycleComponent";

const config = Platform.select({
    web: {headerMode: 'none'},
    default: {
        headerMode: 'none',
        mode: 'modal',
        headerBackTitle: null,
        defaultNavigationOptions: {
            gesturesEnabled: true,
        },
        // header: null,
    },
});

const HomeStack = createStackNavigator(
    {
        Home: HomeScreen,
    },

);
const LinksStack = createStackNavigator(
    {
        Links: LinksScreen,
    },
    config
);

const SettingsStack = createStackNavigator(
    {
        Settings: SettingsScreen,
    },
    config
);

const IndexStack = createStackNavigator(
    {
        Index: {
            screen:IndexScreen,
            navigationOptions: ({ navigation }) => ({
                title: "Demo project",
            }),
        },
    },
    // config,
);

const WebStack = createStackNavigator(
    {
        Web: WebScreen,
    },
    config
);

const BasicFlatListStack = createStackNavigator(
    {
        FlatList: FlatListScreen,
    },
    config
);

const BasicFlatListRemoteStack = createStackNavigator(
    {
        BasicFlatListRemote
    },
    config
);

const LifeCycleStack = createStackNavigator(
    {
        LifeCycleComponent
    },
    config
);

// remember add new stacks here, otherwise it will not store
const demoNavigator = createStackNavigator({
        IndexStack,
        HomeStack,
        LinksStack,
        SettingsStack,
        WebStack,
        BasicFlatListStack,
        BasicFlatListRemoteStack,
        LifeCycleStack
    },
    // config
);

export default demoNavigator;

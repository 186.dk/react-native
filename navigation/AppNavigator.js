import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

// Default tab navigator
// import MainTabNavigator from './MainTabNavigator';

// custom list navigator
import MainTabNavigator from './DemoNavigator';

export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Main: MainTabNavigator,
  })
);

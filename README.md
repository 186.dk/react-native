#React Native Demo 

React Native is a JavaScript framework for building native mobile apps. It uses the React framework and offers large amount of inbuilt components and APIs.

This demo project is combine Expo basic framework and tutorials from https://www.youtube.com/c/NguyenDucHoang

## Start
Run 
```
npm start or expo start
```

## Install library 
For example:
```
npm add react-native-button
```

## How to do:
### Add new menu
0. Create new stack in DemoNavigator.js like:
0. Add new stack in StackNavigator
0. Add ItemLink in LinkItems in IndexScreen.js



## Note
* Change project folders maybe need run npm start --reset-cache

##Log

v0.0.11
1. Refectory navigators name 

v0.0.10
1. Add basic flat list tutorial 

v0.0.9
1. Add flatList remote demo

v0.0.8
1. Add data list json 

v0.0.7 
1. Add lifeCycleComponent demo

v0.0.6
1. Change default buttonTabNavigator to demoNavigator
2. Git ignore .idea

v0.0.5
1. Add edit function to flat list

v0.0.4
1. Add add function to flat list
2. Add doc

v0.0.3
1. Add delete function on flat list

v0.0.2
1. Add flatList demo

v0.0.1 
1. Initial project 
2. Make demo page of webview